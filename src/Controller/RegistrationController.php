<?php

namespace App\Controller;

use App\Domain\Model\Pessoa;
use App\Domain\Model\Usuario;
use App\Infrastructure\Repository\UsuarioRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("api", name="api_")
 */
class RegistrationController extends AbstractFOSRestController
{
    /**
     * @var UsuarioRepository
     */
    private $usuarioRepository;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        UsuarioRepository $usuarioRepository, 
        UserPasswordEncoderInterface $passwordEncoder, 
        EntityManagerInterface $entityManager
    ) {
        $this->usuarioRepository = $usuarioRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function index(Request $request)
    {
        $login = $request->get('username');
        $password = $request->get('password');

        $user = $this->usuarioRepository->findOneBy([
            'login' => $login,
        ]);

        if (!is_null($user)) {
            return $this->view([
                'message' => 'User already exists'
            ], Response::HTTP_CONFLICT);
        }

        $user = new Usuario();
        $pessoa = $this->getDoctrine()->getRepository(Pessoa::class)->find(1);
        $user->setPessoa($pessoa);
        $user->setLogin($login);
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $password)
        );
        $user->createdAt();
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->view($user, Response::HTTP_CREATED)->setContext((new Context())->setGroups(['public']));
    }
}
